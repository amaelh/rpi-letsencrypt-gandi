
# Let's Encrypt Gandi Plugin docker image

**letsencrypt-gandi** is a plugin for [certbot](https://certbot.eff.org/) that allows you to obtain certificates from [Let's Encrypt](https://letsencrypt.org/) and use them with [Gandi](https://www.gandi.net) products such as [Simple Hosting](https://gandi.net/hosting/simple).

**rpi-letsencrypt-gandi** encapsulates all that in a docker container for raspberry pi.

## Requirements

* [**docker**](https://www.docker.com/) installed on your raspberry pi
* A [Gandi API Key](https://wiki.gandi.net/xml-api/activate), which you can get from your [Gandi Account](https://www.gandi.net/admin/api_key)
* You must [add the certificate's domain name to your instance's VHOSTS](https://wiki.gandi.net/simple/shs-dns_config)
* You need to have [SSH Key authentication](https://wiki.gandi.net/en/simple/ssh_key) setup on the Simple Hosting instance
* Your SSH Key must be added to your local `ssh-agent` (use `ssh-add /path/to/key` to add it)

## Installation

Clone this repository on you raspberry pi and build the docker image :

```
~ $ git clone https://github.com/Amaelh/rpi-letsencrypt-gandi.git
~ $ cd rpi-letsencrypt-gandi
~/rpi-letsencrypt-gandi $ docker build -t amaelh/rpi-letsencrypt-gandi:latest .
```

## Usage (Simple Hosting example)

Configure the first three variables, then run docker image with the following command. The certificate will be deployed directly on your instance.

```
~ $ export GANDI_API_KEY="YOUR_API_KEY_HERE"
~ $ export GANDI_INSTANCE_NAME="YOUR_INSTANCE_NAME_HERE"
~ $ export ADMIN_EMAIL="YOUR_EMAIL_HERE"
~ $ 
~ $ docker run -it --rm -v /home/pi/.ssh:/root/.ssh -v /home/pi/rpi-letsencrypt-gandi/etc:/etc/letsencrypt amaelh/rpi-letsencrypt-gandi run --domain $WEBHOST --authenticator letsencrypt-gandi:gandi-shs --letsencrypt-gandi:gandi-shs-name $GANDI_INSTANCE_NAME --letsencrypt-gandi:gandi-shs-vhost $WEBHOST --letsencrypt-gandi:gandi-shs-api-key $GANDI_API_KEY --installer letsencrypt-gandi:gandi-shs --email $ADMIN_EMAIL --text --agree-tos  --non-interactive 
```

